#!bin/sh

TASK_ID=curl -s "$ECS_CONTAINER_METADATA_URI_V4/task" \
            | jq -r ".TaskARN" \
            | cut -d "/" -f 3

TASK_DEFINITION_FAMILY=curl -s "$ECS_CONTAINER_METADATA_URI_V4/task" \
  | jq -r ".Family"

sed -i -e "s/\$TASK_ID/$TASK_ID/g" './grafana-agent.yaml'

sed -i -e "s/\$TASK_DEFINITION_FAMILY/$TASK_DEFINITION_FAMILY/g" './grafana-agent.yaml'

sed -i -e "s/\$AGENT_API_KEY/$AGENT_API_KEY/g" './grafana-agent.yaml'

sed -i -e "s/\$PROMETHEUS_USERNAME/$PROMETHEUS_USERNAME/g" './grafana-agent.yaml'

sudo mv -f './grafana-agent.yaml' '/etc/agent/grafana-agent.yaml'

/bin/agent --config.file=/etc/agent/grafana-agent.yaml --metrics.wal-directory=/etc/agent/data