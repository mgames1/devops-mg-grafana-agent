FROM gitlab.com:443/mgames1/dependency_proxy/containers/grafana/agent@sha256:2172994813962e190a73a1a2f6112de76a7e631d64632940ff9d4df2f45d3af0

ARG AGENT_API_KEY
ENV AGENT_API_KEY=${AGENT_API_KEY}

ARG PROMETHEUS_USERNAME
ENV PROMETHEUS_USERNAME=${PROMETHEUS_USERNAME}

RUN apt-get update && apt-get install -y \
  curl \
  jq \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /etc/agent

COPY . .

EXPOSE 12345

ENTRYPOINT ["/etc/agent/grafana-entrypoint.sh"]